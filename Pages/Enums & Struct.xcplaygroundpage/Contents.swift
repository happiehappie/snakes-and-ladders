//: [Previous](@previous) Class & Initializers
/*:
# Enums & Structs
- Enumerations are great for specifying a finite set of values
- In Swift, we can do more than just specifying the values, because enums can also have methods
- They work great with the `switch` statement, which checks that your switch cases are exhaustive (covers everything)
*/

import Foundation

enum Gears: Int {
    case Neutral = 0;
    case One, Two, Three, Four;
    case Park;
    case Reverse;

    func maxSpeed() -> Double {
        switch (self) {
        case .Neutral, .Park:
            return 0.0
        case .One:
            return 10
        case .Two:
            return 30
        case .Three:
            return 60
        case .Four:
            return 100
        case .Reverse:
            return -50
        }
    }
}

var gear: Gears = Gears.Two
var fastestSpeed: Double = gear.maxSpeed()

/*:
### Struct: 
>   Similar to a class, but always copied when passing around, whereas classes are passed by reference
- Eg: CGRect is a struct
*/
import UIKit

var rectA: CGRect = CGRect(x: 0, y: 0, width: 300, height: 50)
var rectB: CGRect = rectA

rectA.size.height *= 2
rectB.size.width *= 2

rectA
rectB

//: [Next](@next)
