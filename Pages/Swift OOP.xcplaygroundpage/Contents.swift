/*: 
# Object-Oriented Programming with Swift
> You should have some experience doing object oriented programming before approaching this course.
> As a quick review:
* A class defines some data and methods that's relevant to it.
* For example, a Vehicle class can have data like maxSpeed, numberOfPassengers, color etc.
* An object is a specific instance of a class. At the parking lot outside the office, there's about 20 "instances" of Vehicle.
* A class can be subclassed. The Vehicle class may have subclasses like Car, Motorcycle, Truck, etc.
* A subclass inherits all the properties of its superclass, and usually is given the chance to override it too.
* Each subclass can be subclassed, creating a object hierarchy.
*/
