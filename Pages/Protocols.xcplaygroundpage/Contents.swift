//: [Previous](@previous) 
/*:
# Protocols
- You can declare a protocol, which is a set of member variables and methods that need to be supported
- Once you have the protocol defined, classes, enums and structs can declare support and adopt the protocol
- It's similar to subclassing, but there's no overriding, rather just implementing what's stated in the protocol
- You can declare the protocols that are adopted by a class right at the start
- But usually, we use `extension` keyword to make a class adopt a protocol. Side benefit is cleaner code organization.
*/
import Foundation

protocol Rentable {
    var rentalPrice: Double {get}
    func rent()
}

class Vehicle {
    var maxPassengers: Int
    var currentSpeed: Double
    
    init() {
        maxPassengers = 0
        currentSpeed = 0.0
    }
    
    func report() {
        print("Vehicle is moving at \(currentSpeed)")
    }
}

class Car: Vehicle {
    var color: String
    
    init(color: String) {
        self.color = color
        
        super.init()
        self.maxPassengers = 5
        self.currentSpeed = 0.0
    }
    
    override func report() {
        print("This \(color) car is cruising at \(currentSpeed)")
    }
    
    deinit {
        self.color = ""
    }
}

extension Car: Rentable {
    var rentalPrice: Double {
        get { return 100.0 }
    }
    
    func rent() {
        print("Renting out this car now")
    }
}

//: `extension` can be used to add methods to any class, not necessarily your own, and not necessary with the aim of adopting an protocol
extension String {
    func lowerUnderscoreCase() -> String {
        return self.lowercaseString.stringByReplacingOccurrencesOfString(" ", withString: "_")
    }
}

"Hello World".lowerUnderscoreCase()


//: You can use protocols in a collection, or as parameter type, etc.
class House: Rentable {
    var pricePerSquareMeter: Double
    var size: Double
    
    init(price: Double, size: Double) {
        self.pricePerSquareMeter = price
        self.size = size
    }
    var rentalPrice: Double {
        get { return size * pricePerSquareMeter }
    }
    
    func rent() {
        print("Renting out this house now")
    }
}

var rentalListing: [Rentable] = []

var myCar: Car = Car(color: "Red")
var myHouse: House = House(price: 50.0, size: 1000)

rentalListing.append(myCar)
rentalListing.append(myHouse)

//: [Next](@next)
