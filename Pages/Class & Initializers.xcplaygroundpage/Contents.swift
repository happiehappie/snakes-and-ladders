//: [Previous](@previous) Swift OOP Review
/*:
# Class and Initializers
*/

//: In Swift, the `class` keyword is used to declare a new class.
class Vehicle {
    var maxPassengers: Int
    var currentSpeed: Double
    
    init() {
        maxPassengers = 0
        currentSpeed = 0.0
    }

    func report() {
        print("Vehicle is moving at \(currentSpeed)")
    }
}

// Create an instance of a class
var myCar: Vehicle = Vehicle()
myCar.maxPassengers = 5
myCar.currentSpeed = 50
myCar.report()

//: Subclassing
class Car: Vehicle {
    var color: String
    
    init(color: String) {
        self.color = color
        
        super.init()
        self.maxPassengers = 5
        self.currentSpeed = 0.0
    }

    override func report() {
        print("This \(color) car is cruising at \(currentSpeed)")
    }

    deinit {
        self.color = ""
    }
}

var myRedCar: Car = Car(color: "Red")
myRedCar.report()

var myBlueCar: Car = Car(color: "Blue")
myRedCar.report()


//: [Next](@next)
