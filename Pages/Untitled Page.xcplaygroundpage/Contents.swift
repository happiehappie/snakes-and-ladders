//: [Previous](@previous)

import Foundation

var str = "Hello, playground"
var testvar = 0

class Player {
    var position: Int
    var number: Int
    
    init(position: Int, number: Int) {
        self.position = position
        self.number = number
    }
    
}

class Tile {
    var position: Int
    var resultPosition: Int
    
    init(position: Int, resultPosition: Int) {
        self.position = position
        self.resultPosition = resultPosition
    }
    
}

// Initial variables (just to get you started)
var numberOfPlayers = 2
var players: [Player] = []
var tiles: [Tile] = []

func createPlayers(position: Int, playerNumber: Int) {
    let player = Player(position: position, number: playerNumber)
    players.append(player)
}

for index in 1...numberOfPlayers {
    createPlayers(1, playerNumber: index)
}
players

// Functions
func boardSetup() {
    
    
    for var i: Int = 1; i <= 100; i++ {
        let board = Tile(position: i, resultPosition: i)
        tiles.append(board)
    }
    tiles[3].resultPosition = 25
    tiles[9].resultPosition = 30
    tiles[29].resultPosition = 10
    tiles[33].resultPosition = 70
    tiles[43].resultPosition = 84
    tiles[45].resultPosition = 22
    tiles[53].resultPosition = 15
    tiles[62].resultPosition = 43
    tiles[65].resultPosition = 100
    tiles[85].resultPosition = 55
    tiles[92].resultPosition = 65
    tiles[97].resultPosition = 35
}



func rollDice() -> Int {
    // Generate a random number between 1 to 6
    // Hint: use the rand() function to get a random number, then limit it to between 1 to 6
    
    
    return Int((rand()%6) + 1)
}

// Pass it a player number, rolled dice, etc
// and print out a description of the move
func printStep(player: Player, diceRoll: Int) {
    
    
    var moveDescription: String
    moveDescription = "Player \(player.number) rolled \(diceRoll)"
    
    //if player win
    if ((player.position + diceRoll) > 99) {
        player.position = 100
        
        moveDescription += " and reached 100! :D"
        
    } else {
        //move the player
        player.position += diceRoll
        
        //if player position is aheard of the board value
        if player.position > tiles[player.position].resultPosition {
            
            moveDescription += " and got bitten by a snake at position \(player.position), hence returning to \(tiles[player.position].resultPosition)"
            player.position = tiles[player.position].resultPosition
            
        } //if player position is lower than board value
        else if player.position < tiles[player.position].resultPosition {
            
            moveDescription += " and climbed the ladder from \(player.position) to \(tiles[player.position].resultPosition)"
            player.position = tiles[player.position].resultPosition
            
        } //just print saying players moved diceroll number of positions
        else {
            moveDescription += " and moved to \(player.position)"
        }
    }
    
    
    print(moveDescription)
    
    
    
}
boardSetup()
var hasWinner: Bool = false


while !hasWinner {
    // For each player, find the current position, roll the dice,
    // set the new position based on the board's snakes and ladders
    // print out the move
    
    for var i: Int = 0; i < players.count; i++ {
        printStep(players[i], diceRoll: rollDice())
        
        if players[i].position >= 100 {
            hasWinner = true
            print("Congratulations! Player \(i + 1) wins the game!")
            
            break
        }
        
        
        
        // Check for winners
        
        
        
        
    }
    // Repeat until winner is found
}


//: [Next](@next)
